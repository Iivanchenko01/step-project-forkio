# STEP PROJECT FORKIO

## Над проектом работали 
* ***Антон Пархоменко***
* ***Тарас Скороход***

### Список использованных технологий


* **browser-sync**
* **gulp**
* **gulp-autoprefixer**
* **gulp-clean**
* **gulp-clean-css**
* **gulp-concat**
* **gulp-imagemin**
* **gulp-jsmin**
* **gulp-sass**

## Выполнение задач каждого из учасников
* **Задание для студента №1**:***Антон Пархоменко***
* **Задание для студента №2**:***Тарас Скороход***

### Ссылка на сайт 
https://anthrasher.gitlab.io/step-project-forkio/


